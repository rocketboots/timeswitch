import unittest
from datetime import datetime

from timeswitch import timeswitch, InvalidExpression


class TimeswitchTest(unittest.TestCase):

    def test_all_time(self):
        switch = timeswitch('-')
        self.assertTrue(switch(datetime(1, 1, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 6, 5, 9, 2, 9)))
        self.assertTrue(switch(datetime(9999, 12, 31, 23, 59, 59)))

    # year-month-day-hm combinations

    def test_le_year(self):
        switch = timeswitch('- 2016')
        self.assertTrue(switch(datetime(2015, 12, 31, 11, 59, 59)))
        self.assertTrue(switch(datetime(2016, 1, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 12, 31, 11, 59, 59)))
        self.assertFalse(switch(datetime(2017, 1, 1, 0, 0, 0)))

    def test_eq_year(self):
        switch = timeswitch('2016')
        self.assertFalse(switch(datetime(2015, 12, 31, 11, 59, 59)))
        self.assertTrue(switch(datetime(2016, 1, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 12, 31, 11, 59, 59)))
        self.assertFalse(switch(datetime(2017, 1, 1, 0, 0, 0)))

    def test_ge_year(self):
        switch = timeswitch('2016 -')
        self.assertFalse(switch(datetime(2015, 12, 31, 11, 59, 59)))
        self.assertTrue(switch(datetime(2016, 1, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 12, 31, 11, 59, 59)))
        self.assertTrue(switch(datetime(2017, 1, 1, 0, 0, 0)))

    def test_range_year(self):
        switch = timeswitch('2016 - 2017')
        self.assertFalse(switch(datetime(2015, 12, 31, 11, 59, 59)))
        self.assertTrue(switch(datetime(2016, 1, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2017, 12, 31, 11, 59, 59)))
        self.assertFalse(switch(datetime(2018, 1, 1, 0, 0, 0)))

    def test_le_year_month(self):
        switch = timeswitch('- 2016 jul')
        self.assertTrue(switch(datetime(2015, 12, 30, 11, 59, 59)))
        self.assertTrue(switch(datetime(2016, 6, 30, 11, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 31, 11, 59, 59)))
        self.assertFalse(switch(datetime(2016, 8, 1, 0, 0, 0)))
        self.assertFalse(switch(datetime(2017, 1, 1, 0, 0, 0)))
        self.assertFalse(switch(datetime(2017, 8, 1, 0, 0, 0)))

    def test_eq_year_month(self):
        switch = timeswitch('2016 jul')
        self.assertFalse(switch(datetime(2015, 7, 30, 11, 59, 59)))
        self.assertFalse(switch(datetime(2016, 6, 30, 11, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 31, 11, 59, 59)))
        self.assertFalse(switch(datetime(2016, 8, 1, 0, 0, 0)))
        self.assertFalse(switch(datetime(2017, 7, 1, 0, 0, 0)))

    def test_ge_year_month(self):
        switch = timeswitch('2016 jul -')
        self.assertFalse(switch(datetime(2015, 6, 1, 0, 0, 0)))
        self.assertFalse(switch(datetime(2015, 7, 1, 0, 0, 0)))
        self.assertFalse(switch(datetime(2015, 8, 1, 0, 0, 0)))
        self.assertFalse(switch(datetime(2016, 6, 30, 11, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 31, 11, 59, 59)))
        self.assertTrue(switch(datetime(2016, 8, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2017, 1, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2017, 7, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2017, 12, 1, 0, 0, 0)))

    def test_range_year_month(self):
        switch = timeswitch('2016 jul - 2016 aug')
        self.assertFalse(switch(datetime(2015, 7, 1, 0, 0, 0)))
        self.assertFalse(switch(datetime(2016, 6, 30, 11, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 8, 31, 11, 59, 59)))
        self.assertFalse(switch(datetime(2016, 9, 1, 0, 0, 0)))
        self.assertFalse(switch(datetime(2017, 8, 1, 0, 0, 0)))

    def test_le_year_month_day(self):
        switch = timeswitch('- 2016 jul 20')
        self.assertTrue(switch(datetime(2015, 1, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2015, 12, 30, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 19, 11, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 11, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 21, 0, 0, 0)))
        self.assertFalse(switch(datetime(2016, 8, 1, 0, 0, 0)))
        self.assertFalse(switch(datetime(2017, 7, 20, 0, 0, 0)))

    def test_eq_year_month_day(self):
        switch = timeswitch('2016 jul 20')
        self.assertFalse(switch(datetime(2015, 7, 20, 0, 0, 0)))
        self.assertFalse(switch(datetime(2016, 6, 20, 0, 0, 0)))
        self.assertFalse(switch(datetime(2016, 7, 19, 11, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 11, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 21, 0, 0, 0)))
        self.assertFalse(switch(datetime(2016, 8, 20, 0, 0, 0)))
        self.assertFalse(switch(datetime(2017, 7, 20, 0, 0, 0)))

    def test_ge_year_month_day(self):
        switch = timeswitch('2016 jul 20 -')
        self.assertFalse(switch(datetime(2015, 7, 20, 0, 0, 0)))
        self.assertFalse(switch(datetime(2015, 12, 31, 0, 0, 0)))
        self.assertFalse(switch(datetime(2016, 6, 20, 0, 0, 0)))
        self.assertFalse(switch(datetime(2016, 7, 19, 11, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 11, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 21, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 8, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2017, 1, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2017, 12, 31, 0, 0, 0)))

    def test_range_year_month_day(self):
        switch = timeswitch('2016 jul 20 - 2016 jul 21')
        self.assertFalse(switch(datetime(2015, 7, 20, 0, 0, 0)))
        self.assertFalse(switch(datetime(2016, 6, 20, 0, 0, 0)))
        self.assertFalse(switch(datetime(2016, 7, 19, 11, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 21, 11, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 22, 0, 0, 0)))
        self.assertFalse(switch(datetime(2016, 8, 21, 0, 0, 0)))
        self.assertFalse(switch(datetime(2017, 7, 21, 0, 0, 0)))

    def test_le_year_month_day_hm(self):
        switch = timeswitch('- 2016 jul 20 10:30')
        self.assertTrue(switch(datetime(2015, 12, 30, 12, 59, 59)))
        self.assertTrue(switch(datetime(2016, 6, 30, 12, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 19, 12, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 9, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 59)))
        self.assertFalse(switch(datetime(2016, 7, 21, 10, 31, 0)))
        self.assertFalse(switch(datetime(2016, 7, 21, 11, 0, 0)))
        self.assertFalse(switch(datetime(2016, 7, 22, 9, 0, 0)))
        self.assertFalse(switch(datetime(2016, 8, 1, 9, 0, 0)))
        self.assertFalse(switch(datetime(2017, 1, 1, 9, 0, 0)))

    def test_eq_year_month_day_hm(self):
        switch = timeswitch('2016 jul 20 10:30')
        self.assertFalse(switch(datetime(2015, 7, 20, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 6, 20, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 19, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 20, 9, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 59)))
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 31, 0)))
        self.assertFalse(switch(datetime(2016, 7, 20, 11, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 21, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 8, 20, 10, 30, 0)))
        self.assertFalse(switch(datetime(2017, 7, 20, 10, 30, 0)))

    def test_ge_year_month_day_hm(self):
        switch = timeswitch('2016 jul 20 10:30 -')
        self.assertFalse(switch(datetime(2015, 7, 20, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 6, 20, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 19, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 20, 9, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 31, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 11, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 21, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 8, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2017, 1, 1, 0, 0, 0)))

    def test_range_year_month_day_hm(self):
        switch = timeswitch('2016 jul 20 10:30 - 2016 jul 20 10:31')
        self.assertFalse(switch(datetime(2015, 7, 20, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 6, 20, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 19, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 20, 9, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 31, 59)))
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 32, 0)))
        self.assertFalse(switch(datetime(2016, 7, 20, 11, 31, 0)))
        self.assertFalse(switch(datetime(2016, 7, 21, 10, 31, 0)))
        self.assertFalse(switch(datetime(2016, 8, 20, 10, 31, 0)))
        self.assertFalse(switch(datetime(2017, 7, 20, 10, 31, 0)))

    def test_le_month_day_hm(self):
        switch = timeswitch('- jul 20 10:30')
        self.assertFalse(switch(datetime(2015, 12, 30, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 6, 30, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 19, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 9, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 59)))
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 31, 0)))
        self.assertFalse(switch(datetime(2016, 7, 20, 11, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 21, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 8, 20, 10, 30, 0)))
        self.assertTrue(switch(datetime(2017, 7, 20, 10, 30, 0)))

    def test_eq_month_day_hm(self):
        switch = timeswitch('jul 20 10:30')
        self.assertTrue(switch(datetime(2015, 7, 20, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 6, 20, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 19, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 20, 9, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 59)))
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 31, 0)))
        self.assertFalse(switch(datetime(2016, 7, 20, 11, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 21, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 8, 20, 10, 30, 0)))
        self.assertTrue(switch(datetime(2017, 7, 20, 10, 30, 0)))

    def test_ge_month_day_hm(self):
        switch = timeswitch('jul 20 10:30 -')
        self.assertTrue(switch(datetime(2015, 12, 30, 23, 59, 59)))
        self.assertFalse(switch(datetime(2016, 6, 30, 23, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 19, 23, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 20, 9, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 31, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 11, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 21, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 8, 1, 0, 0, 0)))
        self.assertFalse(switch(datetime(2017, 1, 1, 0, 0, 0)))

    def test_range_month_day_hm(self):
        switch = timeswitch('jul 20 10:30 - jul 20 10:31')
        self.assertTrue(switch(datetime(2015, 7, 20, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 6, 20, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 19, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 20, 9, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 31, 59)))
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 32, 0)))
        self.assertFalse(switch(datetime(2016, 7, 20, 11, 31, 59)))
        self.assertFalse(switch(datetime(2016, 7, 21, 10, 31, 59)))
        self.assertFalse(switch(datetime(2016, 8, 20, 10, 31, 59)))
        self.assertTrue(switch(datetime(2017, 7, 20, 10, 31, 59)))

    def test_le_day_hm(self):
        switch = timeswitch('- 20 10:30')
        self.assertTrue(switch(datetime(2015, 7, 20, 10, 30, 59)))
        self.assertTrue(switch(datetime(2016, 6, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 6, 1, 23, 59, 59)))
        self.assertFalse(switch(datetime(2016, 6, 30, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 19, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 9, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 59)))
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 31, 0)))
        self.assertFalse(switch(datetime(2016, 7, 20, 11, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 21, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 8, 20, 10, 30, 0)))
        self.assertTrue(switch(datetime(2017, 7, 20, 10, 30, 0)))

    def test_eq_day_hm(self):
        switch = timeswitch('20 10:30')
        self.assertTrue(switch(datetime(2015, 7, 20, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 6, 20, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 19, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 20, 9, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 59)))
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 31, 0)))

    def test_ge_day_hm(self):
        switch = timeswitch('20 10:30 -')
        self.assertTrue(switch(datetime(2015, 7, 20, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 6, 20, 23, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 19, 23, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 20, 9, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 31, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 11, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 21, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 8, 20, 10, 30, 0)))
        self.assertTrue(switch(datetime(2017, 7, 20, 10, 30, 0)))

    def test_range_day_hm(self):
        switch = timeswitch('20 10:30 - 10:31')
        self.assertTrue(switch(datetime(2016, 6, 20, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 19, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 20, 9, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 31, 59)))
        self.assertFalse(switch(datetime(2016, 7, 21, 10, 32, 0)))

    def test_le_hm(self):
        switch = timeswitch('- 10:30')
        self.assertFalse(switch(datetime(2016, 7, 19, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 9, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 59)))
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 31, 0)))
        self.assertFalse(switch(datetime(2016, 7, 20, 11, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 21, 0, 0, 0)))

    def test_eq_hm(self):
        switch = timeswitch('10:30')
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 59)))
        self.assertFalse(switch(datetime(2016, 7, 21, 10, 31, 0)))

    def test_ge_hm(self):
        switch = timeswitch('10:30 -')
        self.assertFalse(switch(datetime(2016, 7, 20, 9, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 31, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 11, 0, 0)))

    def test_range_hm(self):
        switch = timeswitch('10:30 - 10:31')
        self.assertFalse(switch(datetime(2016, 7, 20, 9, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 31, 59)))
        self.assertFalse(switch(datetime(2016, 7, 21, 10, 32, 0)))
        self.assertFalse(switch(datetime(2016, 7, 21, 11, 30, 0)))

    def test_ge_month_day(self):
        switch = timeswitch('jul 20 -')
        self.assertTrue(switch(datetime(2015, 12, 30, 23, 59, 59)))
        self.assertFalse(switch(datetime(2016, 6, 30, 23, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 19, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 21, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 8, 1, 0, 0, 0)))
        self.assertFalse(switch(datetime(2017, 1, 1, 0, 0, 0)))

    def test_range_month_day(self):
        switch = timeswitch('jul 20 - jul 21')
        self.assertTrue(switch(datetime(2015, 7, 20, 0, 0, 0)))
        self.assertFalse(switch(datetime(2016, 6, 20, 0, 0, 0)))
        self.assertFalse(switch(datetime(2016, 7, 19, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 21, 23, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 22, 0, 0, 0)))
        self.assertFalse(switch(datetime(2016, 8, 21, 0, 0, 0)))

    def test_le_month_day(self):
        switch = timeswitch('- jul 20')
        self.assertFalse(switch(datetime(2015, 12, 30, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 6, 30, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 19, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 23, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 21, 0, 0, 0)))
        self.assertFalse(switch(datetime(2016, 8, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2017, 7, 1, 0, 0, 0)))

    def test_eq_month_day(self):
        switch = timeswitch('jul 20')
        self.assertFalse(switch(datetime(2016, 6, 20, 23, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 19, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 23, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 21, 0, 0, 0)))

    # year-month-weekday-hm combinations

    def test_le_year_month_weekday_hm(self):
        # Expands to 1st Wednesday of the month (because month is provided)
        switch = timeswitch('- 2016 jul wed 10:30')
        self.assertTrue(switch(datetime(2015, 12, 30, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 6, 30, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 5, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 6, 9, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 6, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 6, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 6, 10, 30, 59)))
        self.assertFalse(switch(datetime(2016, 7, 6, 10, 31, 0)))
        self.assertFalse(switch(datetime(2016, 7, 6, 11, 0, 0)))
        self.assertFalse(switch(datetime(2016, 7, 7, 0, 0, 0)))
        self.assertFalse(switch(datetime(2016, 7, 13, 10, 30, 0)))  # another Wednesday
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 30, 0)))  # another Wednesday
        self.assertFalse(switch(datetime(2016, 8, 1, 0, 0, 0)))
        self.assertFalse(switch(datetime(2017, 1, 1, 0, 0, 0)))

    def test_eq_year_month_weekday_hm(self):
        switch = timeswitch('2016 jul wed 10:30')
        self.assertFalse(switch(datetime(2015, 7, 6, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 6, 6, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 5, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 6, 9, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 6, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 6, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 6, 10, 30, 59)))
        self.assertFalse(switch(datetime(2016, 7, 7, 10, 31, 0)))
        self.assertFalse(switch(datetime(2016, 7, 13, 10, 30, 0)))  # another Wednesday
        self.assertFalse(switch(datetime(2016, 7, 27, 10, 30, 59)))  # another Wednesday
        self.assertFalse(switch(datetime(2016, 8, 3, 10, 30, 0)))  # first Wed of Aug
        self.assertFalse(switch(datetime(2016, 8, 6, 10, 30, 0)))

    def test_ge_year_month_weekday_hm(self):
        switch = timeswitch('2016 jul wed 10:30 -')
        self.assertFalse(switch(datetime(2016, 7, 6, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 6, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 6, 10, 30, 59)))
        self.assertTrue(switch(datetime(2016, 7, 6, 10, 31, 0)))

    def test_range_year_month_weekday_hm(self):
        switch = timeswitch('2016 jul wed 10:30 - 2016 jul wed 10:31')
        self.assertFalse(switch(datetime(2015, 7, 6, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 6, 6, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 5, 10, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 6, 9, 30, 0)))
        self.assertFalse(switch(datetime(2016, 7, 6, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 6, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 6, 10, 31, 59)))
        self.assertFalse(switch(datetime(2016, 7, 6, 10, 32, 0)))

    def test_le_month_weekday_hm(self):
        switch = timeswitch('- jul wed 10:30')
        self.assertFalse(switch(datetime(2015, 12, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 1, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 6, 30, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 6, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 6, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 6, 10, 30, 59)))
        self.assertFalse(switch(datetime(2016, 7, 6, 10, 31, 0)))

    def test_eq_month_weekday_hm(self):
        switch = timeswitch('jul wed 10:30')
        self.assertFalse(switch(datetime(2016, 7, 6, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 6, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 6, 10, 30, 59)))
        self.assertFalse(switch(datetime(2016, 7, 6, 10, 31, 0)))
        self.assertFalse(switch(datetime(2016, 7, 13, 10, 30, 0)))  # 2nd Wednesday not matched

    def test_ge_month_weekday_hm(self):
        switch = timeswitch('jul wed 10:30 -')
        self.assertFalse(switch(datetime(2016, 7, 6, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 6, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 6, 10, 30, 59)))
        self.assertTrue(switch(datetime(2016, 7, 6, 10, 31, 0)))

    def test_range_month_weekday_hm(self):
        switch = timeswitch('jul wed 10:30 - jul wed 10:31')
        self.assertFalse(switch(datetime(2016, 7, 6, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 6, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 6, 10, 31, 59)))
        self.assertFalse(switch(datetime(2016, 7, 6, 10, 32, 0)))

    def test_le_weekday_hm(self):
        switch = timeswitch('- wed 10:30')
        # Expands to 'mon 00:00 - wed 10:30'
        # 1st (partial) week in July
        self.assertFalse(switch(datetime(2016, 7, 1, 0, 0, 0)))  # Friday
        self.assertFalse(switch(datetime(2016, 7, 3, 23, 59, 59)))  # Sunday (end of 1st week)
        # 1st full week in July
        self.assertTrue(switch(datetime(2016, 7, 4, 0, 0, 0)))  # Monday (start of week)
        self.assertTrue(switch(datetime(2016, 7, 4, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 6, 10, 30, 59)))  # Wednesday
        self.assertFalse(switch(datetime(2016, 7, 6, 10, 31, 0)))
        self.assertFalse(switch(datetime(2016, 7, 10, 23, 59, 59)))  # Sunday
        # 2nd full week in July
        self.assertTrue(switch(datetime(2016, 7, 11, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 13, 10, 30, 59)))
        self.assertFalse(switch(datetime(2016, 7, 13, 10, 31, 0)))
        self.assertFalse(switch(datetime(2016, 7, 17, 23, 59, 59)))
        # 3rd full week in July
        self.assertTrue(switch(datetime(2016, 7, 18, 0, 0, 0)))

    def test_eq_weekday_hm(self):
        switch = timeswitch('wed 10:30')
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 59)))
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 31, 0)))

    def test_ge_weekday_hm(self):
        switch = timeswitch('wed 10:30 -')
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 31, 0)))

    def test_range_weekday_hm(self):
        switch = timeswitch('wed 10:30 - wed 10:31')
        # 1st Wednesday in July
        self.assertFalse(switch(datetime(2016, 7, 6, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 6, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 6, 10, 31, 59)))
        self.assertFalse(switch(datetime(2016, 7, 6, 10, 32, 0)))
        # Thursday
        self.assertFalse(switch(datetime(2016, 7, 7, 10, 30, 0)))
        # 3rd Wedneday in July
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 29, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 30, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 10, 31, 59)))
        self.assertFalse(switch(datetime(2016, 7, 20, 10, 32, 0)))

    def test_ge_weekday(self):
        switch = timeswitch('wed -')
        self.assertFalse(switch(datetime(2016, 7, 19, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 0, 0, 0)))  # Wednesday
        self.assertTrue(switch(datetime(2016, 7, 20, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 21, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 24, 23, 59, 59)))  # Sunday
        self.assertFalse(switch(datetime(2016, 7, 25, 0, 0, 0)))

    def test_range_weekday(self):
        switch = timeswitch('wed - thu')
        self.assertFalse(switch(datetime(2016, 7, 19, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 21, 23, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 22, 0, 0, 0)))

    def test_le_weekday(self):
        switch = timeswitch('- wed')
        self.assertFalse(switch(datetime(2016, 7, 17, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 18, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 19, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 23, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 21, 0, 0, 0)))

    def test_eq_weekday(self):
        switch = timeswitch('wed')
        self.assertFalse(switch(datetime(2016, 7, 19, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 23, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 21, 0, 0, 0)))

    # Note following defaults to 1st Wednesday (6th July in 2016)

    def test_ge_month_weekday(self):
        switch = timeswitch('jul wed -')
        self.assertFalse(switch(datetime(2016, 7, 5, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 6, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 6, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 7, 0, 0, 0)))

    def test_range_month_weekday(self):
        switch = timeswitch('jul wed - jul thu')
        self.assertFalse(switch(datetime(2016, 7, 5, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 6, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 7, 23, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 8, 0, 0, 0)))

    def test_le_month_weekday(self):
        switch = timeswitch('- jul wed')
        self.assertTrue(switch(datetime(2016, 7, 5, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 6, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 6, 23, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 7, 0, 0, 0)))

    def test_eq_month_weekday(self):
        switch = timeswitch('jul wed')
        self.assertFalse(switch(datetime(2016, 7, 5, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 6, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 6, 23, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 7, 0, 0, 0)))

    # Note following specify ordinal for weekday

    def test_ge_month_ord_weekday(self):
        switch = timeswitch('jul 1st wed -')
        self.assertFalse(switch(datetime(2016, 7, 5, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 6, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 6, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 7, 0, 0, 0)))

    def test_range_month_ord_weekday(self):
        switch = timeswitch('jul 2nd wed - jul 3rd mon')
        self.assertFalse(switch(datetime(2016, 7, 12, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 13, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 18, 23, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 19, 0, 0, 0)))

    def test_le_month_ord_weekday(self):
        switch = timeswitch('- jul 3rd wed')
        self.assertTrue(switch(datetime(2016, 6, 30, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 19, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 20, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 20, 23, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 21, 0, 0, 0)))

    def test_eq_month_ord_weekday(self):
        switch = timeswitch('jul 4th wed')
        self.assertFalse(switch(datetime(2016, 7, 26, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 27, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 27, 23, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 28, 0, 0, 0)))

    # Test ordinal weekday

    def test_ge_ordinal_weekday(self):
        switch = timeswitch('3rd wed -')
        self.assertFalse(switch(datetime(2017, 3, 21, 23, 59, 59)))
        self.assertTrue(switch(datetime(2017, 3, 22, 0, 0, 0)))
        self.assertTrue(switch(datetime(2017, 3, 22, 23, 59, 59)))
        self.assertTrue(switch(datetime(2017, 3, 23, 0, 0, 0)))

    def test_range_ordinal_weekday(self):
        switch = timeswitch('3rd wed - 4th mon')
        self.assertFalse(switch(datetime(2017, 3, 21, 23, 59, 59)))
        self.assertTrue(switch(datetime(2017, 3, 22, 0, 0, 0)))
        self.assertTrue(switch(datetime(2017, 3, 27, 23, 59, 59)))
        self.assertFalse(switch(datetime(2017, 3, 28, 0, 0, 0)))

    def test_le_ordinal_weekday(self):
        switch = timeswitch('- 4th mon')
        self.assertTrue(switch(datetime(2017, 3, 21, 23, 59, 59)))
        self.assertTrue(switch(datetime(2017, 3, 22, 0, 0, 0)))
        self.assertTrue(switch(datetime(2017, 3, 27, 23, 59, 59)))
        self.assertFalse(switch(datetime(2017, 3, 28, 0, 0, 0)))

    def test_eq_ordinal_weekday(self):
        switch = timeswitch('3rd wed')
        self.assertFalse(switch(datetime(2017, 3, 21, 23, 59, 59)))
        self.assertTrue(switch(datetime(2017, 3, 22, 0, 0, 0)))
        self.assertTrue(switch(datetime(2017, 3, 22, 23, 59, 59)))
        self.assertFalse(switch(datetime(2017, 3, 23, 0, 0, 0)))

    # Boolean expressions

    def test_or_op(self):
        switch = timeswitch('2016 or 2018')
        self.assertFalse(switch(datetime(2015, 12, 31, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 1, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 12, 31, 23, 59, 59)))
        self.assertFalse(switch(datetime(2017, 1, 1, 0, 0, 0)))
        self.assertFalse(switch(datetime(2017, 12, 31, 23, 59, 59)))
        self.assertTrue(switch(datetime(2018, 1, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2018, 12, 31, 23, 59, 59)))
        self.assertFalse(switch(datetime(2019, 1, 1, 0, 0, 0)))

    def test_or_op_x3(self):
        switch = timeswitch('dec 25 or dec 27 or dec 29')
        self.assertFalse(switch(datetime(2016, 12, 24)))
        self.assertTrue(switch(datetime(2016, 12, 25)))
        self.assertFalse(switch(datetime(2016, 12, 26)))
        self.assertTrue(switch(datetime(2016, 12, 27)))
        self.assertFalse(switch(datetime(2016, 12, 28)))
        self.assertTrue(switch(datetime(2016, 12, 29)))
        self.assertFalse(switch(datetime(2016, 12, 30)))

    def test_and_op(self):
        switch = timeswitch('2016 and jul')
        self.assertFalse(switch(datetime(2016, 6, 30, 11, 59, 59)))
        self.assertTrue(switch(datetime(2016, 7, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 31, 11, 59, 59)))
        self.assertFalse(switch(datetime(2016, 8, 1, 0, 0, 0)))
        self.assertFalse(switch(datetime(2017, 7, 1, 0, 0, 0)))

    def test_not_op(self):
        switch = timeswitch('2016 and not jul')
        self.assertFalse(switch(datetime(2015, 12, 31, 11, 59, 59)))
        self.assertTrue(switch(datetime(2016, 1, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 6, 30, 11, 59, 59)))
        self.assertFalse(switch(datetime(2016, 7, 1, 0, 0, 0)))
        self.assertFalse(switch(datetime(2016, 7, 31, 11, 59, 59)))
        self.assertTrue(switch(datetime(2016, 8, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 12, 31, 11, 59, 59)))
        self.assertFalse(switch(datetime(2017, 1, 1, 0, 0, 0)))

    # Normal boolean operator precedence is not, and, or

    def test_precedence(self):
        # Equivalent to: not(feb) or (apr and 2016)
        switch = timeswitch('not feb or apr and 2016')
        self.assertTrue(switch(datetime(1, 1, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(1, 1, 31, 23, 59, 59)))
        self.assertFalse(switch(datetime(1, 2, 1, 0, 0, 0)))
        self.assertFalse(switch(datetime(1, 2, 28, 23, 59, 59)))
        self.assertTrue(switch(datetime(1, 7, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 7, 1, 0, 0, 0)))
        self.assertFalse(switch(datetime(2016, 2, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(1, 3, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(9999, 12, 31, 23, 59, 59)))

    # Try reversing that with parenthesis

    def test_parens(self):
        switch = timeswitch('not((feb or apr) and 2016)')
        self.assertTrue(switch(datetime(1, 1, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 1, 31, 23, 59, 59)))
        self.assertFalse(switch(datetime(2016, 2, 1, 0, 0, 0)))
        self.assertFalse(switch(datetime(2016, 2, 29, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 3, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 3, 31, 23, 59, 59)))
        self.assertFalse(switch(datetime(2016, 4, 1, 0, 0, 0)))
        self.assertFalse(switch(datetime(2016, 4, 30, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 5, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2017, 2, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2017, 2, 28, 23, 59, 59)))
        self.assertTrue(switch(datetime(2017, 3, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2017, 3, 31, 23, 59, 59)))
        self.assertTrue(switch(datetime(2017, 4, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2017, 4, 30, 23, 59, 59)))
        self.assertTrue(switch(datetime(2017, 5, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(9999, 12, 31, 23, 59, 59)))

    def test_operators_combined_and_or_ranges(self):
        # Weekday operation with Thursday late night shopping
        switch = timeswitch("(mon-fri and 09:30-18:00) or (thu and 09:30-20:30)")

        # Sunday, closed
        self.assertFalse(switch(datetime(2016, 12, 18, 12)))

        # Monday, normal weekday hours
        self.assertFalse(switch(datetime(2016, 12, 19, 9, 29, 59)))
        self.assertTrue(switch(datetime(2016, 12, 19, 9, 30)))
        self.assertTrue(switch(datetime(2016, 12, 19, 12)))
        self.assertTrue(switch(datetime(2016, 12, 19, 18, 0, 59)))
        self.assertFalse(switch(datetime(2016, 12, 19, 18, 1)))

        # Thursday late night shopping
        self.assertFalse(switch(datetime(2016, 12, 22, 9, 29, 59)))
        self.assertTrue(switch(datetime(2016, 12, 22, 9, 30)))
        self.assertTrue(switch(datetime(2016, 12, 22, 12)))
        self.assertTrue(switch(datetime(2016, 12, 22, 19, 59, 59)))
        self.assertTrue(switch(datetime(2016, 12, 22, 20)))
        self.assertTrue(switch(datetime(2016, 12, 22, 20, 29, 59)))
        self.assertTrue(switch(datetime(2016, 12, 22, 20, 30, 59)))
        self.assertFalse(switch(datetime(2016, 12, 22, 20, 31)))

        # Friday, normal weekday hours
        self.assertFalse(switch(datetime(2016, 12, 23, 9, 29, 59)))
        self.assertTrue(switch(datetime(2016, 12, 23, 9, 30)))
        self.assertTrue(switch(datetime(2016, 12, 23, 12)))
        self.assertTrue(switch(datetime(2016, 12, 23, 18, 0, 59)))
        self.assertFalse(switch(datetime(2016, 12, 23, 18, 1)))

        # Saturday, closed
        self.assertFalse(switch(datetime(2016, 12, 24, 12)))

    def test_operators_combined_not_and_or_ranges(self):
        # Out-of-hours for weekday operation with Thursday late night shopping
        switch = timeswitch("not((mon-fri and 09:30-18:00) or (thu and 09:30-20:30))")

        # Sunday, closed
        self.assertTrue(switch(datetime(2016, 12, 18, 12)))

        # Monday, normal weekday hours
        self.assertTrue(switch(datetime(2016, 12, 19, 9, 29, 59)))
        self.assertFalse(switch(datetime(2016, 12, 19, 9, 30)))
        self.assertFalse(switch(datetime(2016, 12, 19, 12)))
        self.assertFalse(switch(datetime(2016, 12, 19, 18, 0, 59)))
        self.assertTrue(switch(datetime(2016, 12, 19, 18, 1)))

        # Thursday late night shopping
        self.assertTrue(switch(datetime(2016, 12, 22, 9, 29, 59)))
        self.assertFalse(switch(datetime(2016, 12, 22, 9, 30)))
        self.assertFalse(switch(datetime(2016, 12, 22, 12)))
        self.assertFalse(switch(datetime(2016, 12, 22, 19, 59, 59)))
        self.assertFalse(switch(datetime(2016, 12, 22, 20)))
        self.assertFalse(switch(datetime(2016, 12, 22, 20, 29, 59)))
        self.assertFalse(switch(datetime(2016, 12, 22, 20, 30, 59)))
        self.assertTrue(switch(datetime(2016, 12, 22, 20, 31)))

        # Friday, normal weekday hours
        self.assertTrue(switch(datetime(2016, 12, 23, 9, 29, 59)))
        self.assertFalse(switch(datetime(2016, 12, 23, 9, 30)))
        self.assertFalse(switch(datetime(2016, 12, 23, 12)))
        self.assertFalse(switch(datetime(2016, 12, 23, 18, 0, 59)))
        self.assertTrue(switch(datetime(2016, 12, 23, 18, 1)))

        # Saturday, closed
        self.assertTrue(switch(datetime(2016, 12, 24, 12)))

    def test_operators_combined_complex(self):
        # Weekday hours (9-6pm),
        # shorter weekend hours (10-4pm),
        # Thursday late night shopping (9am-9pm);
        # exceptions for Christmas (closed) and Boxing Day (use weekend hours)
        switch = timeswitch(
            """
            (
                (mon-fri and 09:00-17:59) or
                (thu and 09:00-20:59) or
                (sat-sun and 10:00-15:59)
            )
            and not(dec 25)
            and (not(dec 26) or 10:00-15:59)
            """)

        # Sunday, normal weekend hours
        self.assertFalse(switch(datetime(2016, 12, 18, 9, 59, 59)))
        self.assertTrue(switch(datetime(2016, 12, 18, 10)))
        self.assertTrue(switch(datetime(2016, 12, 18, 12)))
        self.assertTrue(switch(datetime(2016, 12, 18, 15, 59, 59)))
        self.assertFalse(switch(datetime(2016, 12, 18, 16)))

        # Monday, normal weekday hours
        self.assertFalse(switch(datetime(2016, 12, 19, 8, 59, 59)))
        self.assertTrue(switch(datetime(2016, 12, 19, 9)))
        self.assertTrue(switch(datetime(2016, 12, 19, 12)))
        self.assertTrue(switch(datetime(2016, 12, 19, 17, 59, 59)))
        self.assertFalse(switch(datetime(2016, 12, 19, 18)))

        # Thursday late night shopping
        self.assertFalse(switch(datetime(2016, 12, 22, 8, 59, 59)))
        self.assertTrue(switch(datetime(2016, 12, 22, 9)))
        self.assertTrue(switch(datetime(2016, 12, 22, 12)))
        self.assertTrue(switch(datetime(2016, 12, 22, 20, 59, 59)))
        self.assertFalse(switch(datetime(2016, 12, 22, 21)))

        # Friday, normal weekday hours
        self.assertFalse(switch(datetime(2016, 12, 23, 8, 59, 59)))
        self.assertTrue(switch(datetime(2016, 12, 23, 9)))
        self.assertTrue(switch(datetime(2016, 12, 23, 12)))
        self.assertTrue(switch(datetime(2016, 12, 23, 17, 59, 59)))
        self.assertFalse(switch(datetime(2016, 12, 23, 18)))

        # Saturday, normal weekend hours
        self.assertFalse(switch(datetime(2016, 12, 24, 9, 59, 59)))
        self.assertTrue(switch(datetime(2016, 12, 24, 10)))
        self.assertTrue(switch(datetime(2016, 12, 24, 12)))
        self.assertTrue(switch(datetime(2016, 12, 24, 15, 59, 59)))
        self.assertFalse(switch(datetime(2016, 12, 24, 16)))

        # Christmas day (Sunday)
        self.assertFalse(switch(datetime(2016, 12, 25, 8)))
        self.assertFalse(switch(datetime(2016, 12, 25, 9)))
        self.assertFalse(switch(datetime(2016, 12, 25, 12)))
        self.assertFalse(switch(datetime(2016, 12, 25, 20)))
        self.assertFalse(switch(datetime(2016, 12, 25, 21)))

        # Boxing day (Monday), using weekend hours
        self.assertFalse(switch(datetime(2016, 12, 24, 9, 59, 59)))
        self.assertTrue(switch(datetime(2016, 12, 24, 10)))
        self.assertTrue(switch(datetime(2016, 12, 24, 12)))
        self.assertTrue(switch(datetime(2016, 12, 24, 15, 59, 59)))
        self.assertFalse(switch(datetime(2016, 12, 24, 16)))

    # Shorthand ranges

    def test_shorthand_range(self):
        switch = timeswitch('2016 jan - feb')
        self.assertFalse(switch(datetime(2015, 12, 31, 23, 59, 59)))
        self.assertTrue(switch(datetime(2016, 1, 1, 0, 0, 0)))
        self.assertTrue(switch(datetime(2016, 2, 29, 23, 59, 59)))
        self.assertFalse(switch(datetime(2016, 3, 1, 0, 0, 0)))
        self.assertFalse(switch(datetime(2017, 1, 1, 0, 0, 0)))

    # Parsing errors

    def test_invalid_year(self):
        self.assertRaises(InvalidExpression, timeswitch, '10000')

    def test_invalid_month_num(self):
        self.assertRaises(InvalidExpression, timeswitch, '2016 13')

    def test_invalid_month(self):
        self.assertRaises(InvalidExpression, timeswitch, '2016 january')

    def test_invalid_day(self):
        self.assertRaises(InvalidExpression, timeswitch, '2016 jan 42')

    def test_invalid_weekday(self):
        self.assertRaises(InvalidExpression, timeswitch, 'jan friday')

    def test_invalid_ordinal(self):
        self.assertRaises(InvalidExpression, timeswitch, 'jan 7th fri')

    def test_invalid_hour(self):
        self.assertRaises(InvalidExpression, timeswitch, '24:00')
        self.assertRaises(InvalidExpression, timeswitch, '25:00')

    def test_invalid_minute(self):
        self.assertRaises(InvalidExpression, timeswitch, '23:76')

    def test_invalid_range(self):
        self.assertRaises(InvalidExpression, timeswitch, '2016 jan - feb 12')
